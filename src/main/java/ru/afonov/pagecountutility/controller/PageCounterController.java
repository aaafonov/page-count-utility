package ru.afonov.pagecountutility.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.afonov.pagecountutility.service.PageCountResult;
import ru.afonov.pagecountutility.service.PageCounterService;

@RestController
public class PageCounterController {
    private final PageCounterService pageCounterService;

    public PageCounterController(PageCounterService pageCounterService) {
        this.pageCounterService = pageCounterService;
    }

    @GetMapping("/count-page")
    public ResponseEntity<PageCountResult> countPages(@RequestParam String path) {
        PageCountResult pageCountResult = pageCounterService.countPages(path);
        return ResponseEntity.ok(pageCountResult);
    }
}
