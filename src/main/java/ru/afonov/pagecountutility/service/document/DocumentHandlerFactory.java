package ru.afonov.pagecountutility.service.document;

import java.io.File;
import java.util.List;

public class DocumentHandlerFactory {
    private final List<DocumentHandler> documentHandlers;

    public DocumentHandlerFactory(List<DocumentHandler> documentHandlers) {
        this.documentHandlers = documentHandlers;
    }

    public DocumentHandler getDocumentHandler(File file) {
        for (DocumentHandler handler : documentHandlers) {
            if (handler.isSupported(file)) {
                return handler;
            }
        }
        return null;
    }
}
