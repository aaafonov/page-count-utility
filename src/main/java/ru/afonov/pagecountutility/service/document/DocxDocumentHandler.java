package ru.afonov.pagecountutility.service.document;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DocxDocumentHandler implements DocumentHandler {
    private static final String DOCX_DOCUMENT_EXTENSION = "docx";

    @Override
    public int countPages(File file) throws DocumentHandlerException {
        try (XWPFDocument document = new XWPFDocument(new FileInputStream(file))) {
            XWPFWordExtractor extractor = new XWPFWordExtractor(document);
            return extractor.getExtendedProperties().getUnderlyingProperties().getPages();
        } catch (IOException e) {
            throw new DocumentHandlerException("Error when counting the number of pages in a DOCX document: " +
                    file.getName(), e);
        }
    }

    @Override
    public boolean isSupported(File file) {
        String extension = getFileExtension(file);
        return extension.equals(DOCX_DOCUMENT_EXTENSION);
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf + 1);
    }
}
