package ru.afonov.pagecountutility.service.document;

import java.io.File;

public interface DocumentHandler {
    int countPages(File file) throws DocumentHandlerException;

    boolean isSupported(File file);
}
