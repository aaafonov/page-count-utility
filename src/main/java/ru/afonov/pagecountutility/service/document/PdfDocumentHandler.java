package ru.afonov.pagecountutility.service.document;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.IOException;

public class PdfDocumentHandler implements DocumentHandler {
    private static final String PDF_DOCUMENT_EXTENSION = "pdf";

    @Override
    public int countPages(File file) throws DocumentHandlerException {
        try (PDDocument document = PDDocument.load(file)) {
            return document.getNumberOfPages();
        } catch (IOException e) {
            throw new DocumentHandlerException("Error when counting the number of pages in a PDF document: " +
                    file.getName(), e);
        }
    }

    @Override
    public boolean isSupported(File file) {
        String extension = getFileExtension(file);
        return extension.equals(PDF_DOCUMENT_EXTENSION);
    }

    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return "";
        }
        return name.substring(lastIndexOf + 1);
    }
}
