package ru.afonov.pagecountutility.service.document;

public class DocumentHandlerException extends Exception {

    public DocumentHandlerException(String message, Throwable cause) {
        super(message, cause);
    }
}
