package ru.afonov.pagecountutility.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.afonov.pagecountutility.service.document.DocumentHandler;
import ru.afonov.pagecountutility.service.document.DocumentHandlerException;
import ru.afonov.pagecountutility.service.document.DocumentHandlerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class PageCounterService {
    private static final Logger logger = LoggerFactory.getLogger(PageCounterService.class);

    private final DocumentHandlerFactory documentHandlerFactory;

    @Autowired
    public PageCounterService(DocumentHandlerFactory documentHandlerFactory) {
        this.documentHandlerFactory = documentHandlerFactory;
    }

    public PageCountResult countPages(String root) {
        File rootDirectory = new File(root);
        int documentCount = 0;
        int pageCount = 0;
        List<File> files = listFiles(rootDirectory);
        try {
            for (File file : files) {
                DocumentHandler documentHandler = documentHandlerFactory.getDocumentHandler(file);
                if (documentHandler != null) {
                    documentCount++;
                    pageCount += documentHandler.countPages(file);
                }
            }
        } catch (DocumentHandlerException e) {
            logger.error("Error while processing the document: {}", e.getMessage());
        }
        logger.info("Document count: {}, page count: {}", documentCount, pageCount);
        return new PageCountResult(documentCount, pageCount);
    }

    private List<File> listFiles(File directory) {
        List<File> fileList = new ArrayList<>();
        if (directory.isFile()) {
            fileList.add(directory);
        } else if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    fileList.addAll(listFiles(file));
                }
            }
        }
        return fileList;
    }
}
