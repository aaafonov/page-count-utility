package ru.afonov.pagecountutility.service;

public record PageCountResult(int documents,
                              int pages) {
}
