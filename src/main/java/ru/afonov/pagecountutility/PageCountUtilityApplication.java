package ru.afonov.pagecountutility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PageCountUtilityApplication {

    public static void main(String[] args) {
        SpringApplication.run(PageCountUtilityApplication.class, args);
    }

}
