package ru.afonov.pagecountutility.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.afonov.pagecountutility.service.document.DocumentHandler;
import ru.afonov.pagecountutility.service.document.DocumentHandlerFactory;
import ru.afonov.pagecountutility.service.document.PdfDocumentHandler;
import ru.afonov.pagecountutility.service.document.DocxDocumentHandler;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean
    public PdfDocumentHandler pdfDocumentHandler() {
        return new PdfDocumentHandler();
    }

    @Bean
    public DocxDocumentHandler docxDocumentHandler() {
        return new DocxDocumentHandler();
    }

    @Bean
    public DocumentHandlerFactory documentHandlerFactory() {
        return new DocumentHandlerFactory(documentHandlers());
    }

    @Bean
    public List<DocumentHandler> documentHandlers() {
        List<DocumentHandler> documentHandlers = new ArrayList<>();
        documentHandlers.add(pdfDocumentHandler());
        documentHandlers.add(docxDocumentHandler());
        return documentHandlers;
    }
}
