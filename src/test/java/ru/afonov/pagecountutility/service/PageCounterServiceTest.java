package ru.afonov.pagecountutility.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import ru.afonov.pagecountutility.service.document.DocumentHandler;
import ru.afonov.pagecountutility.service.document.DocumentHandlerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

class PageCounterServiceTest {
    @TempDir
    File tempDir;

    private DocumentHandlerFactory documentHandlerFactory;

    private PageCounterService pageCounterService;

    @BeforeEach
    void setUp() {
        documentHandlerFactory = mock(DocumentHandlerFactory.class);
        pageCounterService = new PageCounterService(documentHandlerFactory);
    }

    @Test
    void successfullyCountDocumentsWithPages() throws Exception {
        final int numberOfPagesInDocxFile = 2;
        final int numberOfPagesInPdfFile = 3;
        File docxFile = createTestFile("test.docx", numberOfPagesInDocxFile);
        File pdfFile = createTestFile("test.pdf", numberOfPagesInPdfFile);

        DocumentHandler mockDocxHandler = mock(DocumentHandler.class);
        given(mockDocxHandler.countPages(docxFile)).willReturn(numberOfPagesInDocxFile);

        DocumentHandler mockPdfHandler = mock(DocumentHandler.class);
        given(mockPdfHandler.countPages(pdfFile)).willReturn(numberOfPagesInPdfFile);

        given(documentHandlerFactory.getDocumentHandler(docxFile)).willReturn(mockDocxHandler);
        given(documentHandlerFactory.getDocumentHandler(pdfFile)).willReturn(mockPdfHandler);

        final int documentsCount = 2;
        PageCountResult expectedPageCountResult =
                new PageCountResult(documentsCount, numberOfPagesInDocxFile + numberOfPagesInPdfFile);


        PageCountResult pageCountResult = pageCounterService.countPages(tempDir.getAbsolutePath());


        assertThat(pageCountResult, equalTo(expectedPageCountResult));
    }

    private File createTestFile(String filename, int numPages) throws IOException {
        File file = new File(tempDir, filename);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            for (int i = 0; i < numPages; i++) {
                fos.write(("Page " + (i + 1)).getBytes());
            }
        }
        return file;
    }
}